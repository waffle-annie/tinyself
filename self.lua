-- copyright 2022 anne redko
-- mit license

-- thanks to newtrality from the r/pl discord
-- originally made in 3-4 hours

--\[ Credit ]----------------------------------------------------------------------------------------
--/[ Types ]-----------------------------------------------------------------------------------------

---@alias TokType "any"|"idn"|"str"|"num"|"nln"|"opr"|"cpr"|"obr"|"cbr"
---@alias Tok { t: TokType, v: string }
---@alias AstType "seq"|"msg"
---@alias Ast_ unknown
---@alias Ast
---| { t: "idn", name: string }
---| { t: "str", value: string }
---| { t: "num", value: number }
---| { t: "seq", body: Ast_[] }
---| { t: "blk", body: Ast_[] }
---| { t: "msg", name: string, target: Ast_, body: Ast_ }
---@alias Obj_ unknown
---@alias Ctx_ unknown
---@alias ObjOnMsg fun(self: Obj_, msg: string, arg: Obj_, ctx: Ctx_): Obj_
---@alias ObjMsg_ fun(self: Obj_, arg: Obj_, ctx: Ctx_): Obj_
---@alias ObjMsg fun(self: Obj, arg: Obj, ctx: Ctx): Obj
---@alias ObjMsgs { [string]: ObjMsg }
---@alias Obj { msgs: { [string]: ObjMsg_ }, ctx: Ctx_, onmsg: ObjOnMsg }
---@alias Ctx { [string]: Obj, [0]: Ctx_ }

--\[ Types ]-----------------------------------------------------------------------------------------
--/[ Lexing ]----------------------------------------------------------------------------------------

--- convert string into tokens
---@param s string
local function lex(s)
	---@type TokType
	local t = "any"
	---@type string
	local b = ""
	---@type Tok[]
	local ts = {}
	---@type boolean
	local stre = false
	---@type boolean
	local nl = false
	local i = 0
	while i <= #s + 1 do
		local c = s:sub(i, i)
		if c:match"%S" then nl = false end
		if t == "any" and c:match "[A-Za-z]" then
			t = "idn" b = c
		elseif t == "any" and c == "-" then
			table.insert(ts, { t = "idn", v = "<nil-literal>" })
		elseif t == "any" and c:match "[0-9]" then
			t = "num" b = c
		elseif t == "any" and c == "(" then
			table.insert(ts, { t = "opr", v = "<open-parenthesis>" })
		elseif t == "any" and c == ")" then
			table.insert(ts, { t = "cpr", v = "<close-parenthesis>" })
		elseif t == "any" and c == "[" then
			table.insert(ts, { t = "obr", v = "<open-bracket>" })
		elseif t == "any" and c == "]" then
			table.insert(ts, { t = "cbr", v = "<close-bracket>" })
		elseif t == "any" and c == "\"" then
			t = "str" b = ""
		elseif t == "idn" then
			if c:match "[A-Za-z0-9-]" then
				b = b .. c
			else
				table.insert(ts, { t = "idn", v = b })
				t = "any"
				i = i - 1
				b = ""
			end
		elseif t == "num" then
			if c:match "[0-9]" then
				b = b .. c
			else
				table.insert(ts, { t = "num", v = b })
				t = "any"
				i = i - 1
				b = ""
			end
		elseif t == "str" and c == "\"" and not stre then
			table.insert(ts, { t = "str", v = b })
			t = "any"
		elseif t == "str" and c == "\\" then
			stre = true
		elseif t == "str" and c ~= "\"" then
			if stre then
				b = b .. ({
					["n"] = "\n",
					["t"] = "\t",
					["\""] = "\"",
					["\'"] = "\'",
					["e"] = "\x1b",
				})[c]
				stre = false
			else
				b = b .. c
			end
		end
		if t ~= "str" and c == "\n" and not nl then
			nl = true
			table.insert(ts, { t = "nln", v = "<newline>" })
		end
		i = i + 1
	end
	return ts
end

--\[ Lexing ]----------------------------------------------------------------------------------------
--/[ Parsing ]---------------------------------------------------------------------------------------

---@type fun(ts: Tok[], i: integer): integer, Ast
local parse_expr

---@param ts Tok[]
---@param i integer
---@return integer, Ast
local function parse_atom(ts, i)
	if ts[i].t == "opr" then
		i = i + 1
		while ts[i].t == "nln" do i = i + 1 end
		local e
		i, e = parse_expr(ts, i)
		while ts[i].t == "nln" do i = i + 1 end
		if ts[i].t ~= "cpr" then
			error("expected a closing parenthesis, got " .. ts[i].t .. " '" .. ts[i].v .. "' instead.")
		end
		return i + 1, e
	elseif ts[i].t == "obr" then
		i = i + 1
		while ts[i].t == "nln" do i = i + 1 end
		local e
		i, e = parse_expr(ts, i)
		if ts[i].t ~= "cbr" then
			error("expected a closing bracket, got " .. ts[i].t .. " '" .. ts[i].v .. "' instead.")
		end
		return i + 1, { t = "blk", body = e }
	elseif ts[i].t == "idn" then
		return i + 1, { t = "idn", name = ts[i].v }
	elseif ts[i].t == "str" then
		return i + 1, { t = "str", value = ts[i].v }
	elseif ts[i].t == "num" then
		return i + 1, { t = "num", value = tonumber(ts[i].v) }
	end
	error("expected an atom, got " .. ts[i].t .. " '" .. ts[i].v .. "' instead.")
	return i + 1, { t = "err" }
end

--[[
	src msg arg kw0 kw1: arg1 kw2: arg2 ...
	(src msg arg) kw0 (((((---) kw1 arg1) kw2 arg2) ...)

if true (inp equals "secret") then do: [
	console write-line "secret!"
] else: [
		console write-line "no secret!"
]

(if true (inp equals "secret")) then (((---) do [
	console write-line "secret!"
]) else [
		console write-line "no secret!"
])

]]

--- parse tokens into an ast
---@param ts Tok[]
---@return integer, Ast
parse_expr = function(ts, i)
	---@type Ast[]
	local es = {}
	repeat
		while ts[i] and ts[i].t == "nln" do i = i + 1 end
		if not ts[i] then break end
		if ts[i].t == "cpr" or ts[i].t == "cbr" then break end

		local tgt
		i, tgt = parse_atom(ts, i)
		if ts[i] and ts[i].t == "nln" then i = i + 1 end

		while ts[i] and ts[i].t == "idn" do
			local msg = ts[i].v
			i = i + 1
			if ts[i] and ts[i].t == "nln" then i = i + 1 end
			local arg
			i, arg = parse_atom(ts, i)
			tgt = { t = "msg", name = msg, target = tgt, body = arg }
		end

		table.insert(es, tgt)
	until i > #ts or ts[i].t ~= "nln"
	return i, { t = "seq", body = es }
end

--- parse tokens into an ast
---@param ts Tok[]
local function parse(ts)
	local _, e = parse_expr(ts, 1)
	return e
end
--- recolor a string using ANSI colors
---@param c boolean? should recolor the string
---@param n string name of the color
---@param e string string to recolor
local function recolor(c, n, e)
	if not c then return e end
	if n == "none" then
		return '\x1b[0m' .. e
	elseif n == "red" then
		return '\x1b[31m' .. e .. '\x1b[0m'
	elseif n == "green" then
		return '\x1b[32m' .. e .. '\x1b[0m'
	elseif n == "blue" then
		return '\x1b[34m' .. e .. '\x1b[0m'
	elseif n == "cyan" then
		return '\x1b[35m' .. e .. '\x1b[0m'
	elseif n == "orange" then
		return '\x1b[33m' .. e .. '\x1b[0m'
	end
	return e
end

AST_FORMAT_LIMIT = 80
AST_FORMAT_INDENT = '  '

--- format ast as source code. this isn't perfect but it works for now.
---@param o Ast the ast node to format
---@param c boolean? should the output have colors (false by default)
---@param i integer? indentation (default is 0)
---@param p boolean? should the output be in parenthesis (internal)
---@return string
local function formatAst(o, c, i, p)
	--- return length of last line
	---@param ast Ast
	---@param indent integer
	---@param paren boolean?
	local function formatAst_(ast, indent, paren)
		local s = formatAst(ast, c, indent, paren)

		---@type integer
		local idx = 0

		---@type string
		local last = nil

		for line in string.gmatch(s, "([^\n]+)") do
			last = line
			idx = idx + 1
		end

		local start, _ = string.find(last, "[^ \t]")
		start = start or 0

		return { indent = math.floor(start / #AST_FORMAT_INDENT), length = #last, s = s }
	end

	i = i or 0
	if o == nil then
		return recolor(c, "red", "<nil>")
	elseif o.t == "err" then
		return recolor(c, "red", "<error>")
	elseif o.t == "idn" then
		if o.name == "<nil-literal>" then return recolor(c, "blue", "-") end
		if c and o.name == "var" then return recolor(c, "blue", o.name) end
		if c and o.name == "if" then return recolor(c, "blue", o.name) end
		if c and o.name == "system" then return recolor(c, "blue", o.name) end
		return o.name
	elseif o.t == "str" then
		return recolor(c, "green", '"' .. o.value .. '"')
	elseif o.t == "num" then
		return recolor(c, "orange", tostring(o.value))
	elseif o.t == "seq" or o.t == "blk" then
		---@type Ast[]
		local body

		---@type { left: string, right: string }
		local paren

		if o.t == "blk" then
			body = o.body.body
			paren = { left = '[', right = ']' }
		else
			body = o.body
			paren = { left = '(', right = ')' }
		end

		local nl = #body > 1
		local s = ''

		if #body == 1 then
			local info = formatAst_(o.body[1], i + 1)
			nl = nl or info.length > AST_FORMAT_LIMIT
		end

		if p then s = s .. paren.left end
		for idx, v in ipairs(body) do
			if nl and #body ~= 1 then
				if p or idx > 1 then s = s .. '\n' end
				s = s .. AST_FORMAT_INDENT:rep(i + 1)
			end
			s = s .. formatAst(v, c, i + 1)
		end
		if nl and p and #body ~= 1 then
			s = s .. '\n'
			s = s .. AST_FORMAT_INDENT:rep(i)
		end
		if p then s = s .. paren.right end
		return s
	elseif o.t == "msg" then
		local name = o.name
		if name == "<nil-literal>" then name = recolor(c, "cyan", "-") end
		if name == "true" then name = recolor(c, "cyan", name) end
		if name == "else" then name = recolor(c, "cyan", name) end
		if name == "else-if" then name = recolor(c, "cyan", name) end
		if name == "then" then name = recolor(c, "cyan", name) end
		if name == "equals" then name = recolor(c, "cyan", name) end

		local s = ''

		if p then s = s .. '(' end

		-- the target always stays on the same line.
		local info = formatAst_(o.target, i, false)
		s = s .. info.s

		local len = info.length

		-- the message can go onto a different line.
		if info.length > AST_FORMAT_LIMIT then
			s = s .. '\n'
			s = s .. AST_FORMAT_INDENT:rep(i + 1)
			len = #AST_FORMAT_INDENT * (i + i)
		else s = s .. ' ' end

		s = s .. name
		len = len + #name

		if len > AST_FORMAT_LIMIT then
			s = s .. '\n'
			s = s .. AST_FORMAT_INDENT:rep(info.indent + 2)
		else s = s .. ' ' end
		s = s .. formatAst(o.body, c, info.indent + 2, true)
		if p then s = s .. ')' end
		return s
	else
		return recolor(c, "red", "<unknown-type-" .. o.t .. ">")
	end
end

--\[ Parsing ]---------------------------------------------------------------------------------------
--/[ Eval ]------------------------------------------------------------------------------------------

--- get a value from a context. error if not found.
---@param ctx Ctx
---@param name string
---@return Obj
local function getValFromCtx(ctx, name)
	if ctx[name] then return ctx[name] end
	if ctx[0] then return getValFromCtx(ctx[0], name) end
	error("value of name '" .. name .. "' not found.")
end

--- set a value from a context. error if not found.
---@param ctx Ctx
---@param name string
---@param value Obj
---@return Obj
local function setValFromCtx(ctx, name, value)
	if ctx[name] then ctx[name] = value end
	if ctx[0] then return setValFromCtx(ctx[0], name, value) end
	error("could not set value of name '" .. name .. "'.")
end


--- make a nil object
---@return Obj
local function makeNilObj()
	return { msgs = {}, ctx = {} }
end

--- clone an object
---@param o Obj
---@return Obj
local function cloneObj(o)
	local n = { msgs = {}, ctx = { [0] = o.ctx } }
	for k, v in pairs(o.msgs) do
		n.msgs[k] = v
	end
	return n
end

---@param o Obj
---@return boolean
local function isTruthy(o)
	if o.ctx["-type"] == "boolean" then return o.ctx["-value"]
	elseif o.ctx["-type"] == "string" then return o.ctx["-value"] ~= ""
	elseif o.ctx["-type"] == "number" then return o.ctx["-value"] ~= 0
	else return false end
end

---@type ObjMsgs
BooleanObjMsgs = {}

--- make a boolean object
---@param b boolean
---@return Obj
local function makeBooleanObj(b)
	return { msgs = BooleanObjMsgs, ctx = { ["-type"] = "boolean", ["-value"] = b }, onmsg = DefaultOnMsg }
end

---@type ObjMsgs
StringObjMsgs = {}

--- make a string object
---@param s string
---@return Obj
local function makeStringObj(s)
	return { msgs = StringObjMsgs, ctx = { ["-type"] = "string", ["-value"] = s }, onmsg = DefaultOnMsg }
end

---@type ObjMsgs
NumberObjMsgs = {}

--- make a number object
---@param n number
---@return Obj
local function makeNumberObj(n)
	return { msgs = NumberObjMsgs, ctx = { ["-type"] = "number", ["-value"] = n }, onmsg = DefaultOnMsg }
end

--- make a context object
---@param c Ctx
---@return Obj
local function makeContextObj(c)
	return {
		msgs = {},
		ctx = { ["-type"] = "context", ["-value"] = c },
		---@param self Obj
		---@param msg string
		onmsg = function(self, msg, _, _)
			if msg == "<nil-literal>" then
				return { -- setter object
					msgs = {},
					ctx = {},
					onmsg = function(_, msg2, arg, _)
						setValFromCtx(self.ctx, msg2, arg)
					end
				}
			end
			return getValFromCtx(self.ctx, msg)
		end
	}
end

---@type ObjMsgs
BlockObjMsgs = {}

--- make a block object
---@param b Ast
---@param ctx Ctx
---@return Obj
local function makeBlockObj(b, ctx)
	return { msgs = BlockObjMsgs, ctx = { ["-type"] = "block", ["-value"] = b, ["--ctx"] = ctx }, onmsg = DefaultOnMsg }
end

--- evaluates an ast node in context.
---@param o Ast
---@param ctx Ctx
---@return Obj
local function eval(o, ctx)
	if o == nil then error("nil eval") end
	if o.t == "err" then
		error("error type")
	elseif o.t == "idn" then
		if o.name == "<nil-literal>" then return getValFromCtx(ctx, "-") end
		return getValFromCtx(ctx, o.name)
	elseif o.t == "str" then
		return makeStringObj(o.value --[[@as string]])
	elseif o.t == "num" then
		return makeNumberObj(o.value --[[@as number]])
	elseif o.t == "seq" then
		local r = makeNilObj()
		for _, v in pairs(o.body) do
			r = eval(v, ctx)
		end
		return r
	elseif o.t == "blk" then
		return makeBlockObj(o.body, ctx)
	elseif o.t == "msg" then
		local tgt = eval(o.target, ctx)
		local msg = eval(o.body --[[@as Ast]], ctx)
		if o.name == "<nil-literal>" then
			return cloneObj(tgt)
		end
		return tgt.onmsg(tgt, o.name, msg, ctx)
	else
		error("unknown type: '" .. o.t .. "'")
	end
end

BooleanObjMsgs = {
	["and"] = function(self, arg, _)
		return makeBooleanObj(isTruthy(self) and isTruthy(arg))
	end,
	["or"] = function(self, arg, _)
		return makeBooleanObj(isTruthy(self) or isTruthy(arg))
	end,
	["equals"] = function(self, arg, _)
		if arg.ctx["-type"] ~= "boolean" then return makeBooleanObj(false) end
		return makeBooleanObj(self.ctx["-value"] == arg.ctx["-value"])
	end
}

NumberObjMsgs = {
	["plus"] = function(self, arg, _)
		if arg.ctx["-type"] ~= "number" then error("Number/plus sent not with a Number.") end
		return makeNumberObj(self.ctx["-value"] + arg.ctx["-value"])
	end,
	["minus"] = function(self, arg, _)
		if arg.ctx["-type"] ~= "number" then error("Number/minus sent not with a Number.") end
		return makeNumberObj(self.ctx["-value"] - arg.ctx["-value"])
	end,
	["times"] = function(self, arg, _)
		if arg.ctx["-type"] ~= "number" then error("Number/times sent not with a Number.") end
		return makeNumberObj(self.ctx["-value"] * arg.ctx["-value"])
	end,
	["divided-by"] = function(self, arg, _)
		if arg.ctx["-type"] ~= "number" then error("Number/divided-by sent not with a Number.") end
		return makeNumberObj(self.ctx["-value"] / arg.ctx["-value"])
	end,
	["equals"] = function(self, arg, _)
		if arg.ctx["-type"] ~= "number" then return makeBooleanObj(false) end
		return makeBooleanObj(self.ctx["-value"] == arg.ctx["-value"])
	end
}

StringObjMsgs = {
	["equals"] = function(self, arg, _)
		return makeBooleanObj(arg.ctx["-type"] == "string" and arg.ctx["-value"] == self.ctx["-value"])
	end
}

BlockObjMsgs = {
	["invoke"] = function(self, arg, _)
		return eval(self.ctx["-value"], { [0] = self.ctx["--ctx"], ['@'] = arg })
	end,
	["in"] = function(self, ctxo, _)
		local newSelf = cloneObj(self)
		if ctxo.ctx["-type"] ~= "context" then
			error("Block/in expects a Context.")
		end
		newSelf.ctx["--ctx"] = { [0] = self.ctx["--ctx"] }
		for k, v in pairs(ctxo.ctx) do
			newSelf.ctx["--ctx"][k] = v
		end
		return newSelf
	end
}

---@param self Obj
---@param arg Obj
function DefaultOnMsg(self, msg, arg, _)
	if self.msgs[msg] == nil then
		if arg.ctx["-type"] ~= "block" then
			error("when a message ('" .. msg .. "') doesn't exist, it's body needs to be provided.")
		end

		---@param arg2 Obj
		---@param ctx Ctx
		self.msgs[msg] = function(_, arg2, ctx)
			---@type Obj
			local o = arg.onmsg(arg, "in", makeContextObj({
				["self"] = makeContextObj(ctx)
			}), ctx)
			return o.onmsg(o, "invoke", arg2, ctx)
		end
		return self
	end
	return self.msgs[msg](self, arg, self.ctx)
end

---@type Ctx
Builtins = {
	["-"] = makeNilObj(),
	["var"] = {
		onmsg = function(_, msg, arg, ctx)
			ctx[msg] = arg
			return ctx[msg]
		end,
		msgs = {},
		ctx = {}
	},
	["if"] = { ctx = {}, onmsg = DefaultOnMsg, msgs = {
		["true"] = function(_, condition, ctx)
			local toEval = nil
			return { ctx = { }, onmsg = DefaultOnMsg, msgs = {
				["then"] = function(self, branch, _)
					if isTruthy(condition) then
						toEval = branch
					end
					return self
				end,
				["else-if"] = function(self, condition2, _)
					condition = condition2
					return self
				end,
				["else"] = function(_, falseBranch, _)
					if toEval == nil then
						toEval = falseBranch
						condition = makeNilObj()
					end
					if toEval.ctx["-type"] ~= "block" then
						error("If/true/else: branch isn't a block!")
					end
					return toEval.onmsg(toEval, "invoke", condition, ctx)
				end
			} }
		end
	} },
	["true"] = makeBooleanObj(true),
	["false"] = makeBooleanObj(false),
	["system"] = {
		ctx = {},
		onmsg = DefaultOnMsg,
		msgs = {
			["exit"] = function(_, code, _)
				if code.ctx["-type"] ~= "number" then error("System/exit expects a number") end
				os.exit(code.ctx["-value"])
				return makeNilObj()
			end,
			["get-console"] = function(_, _, _)
				return {
					ctx = {},
					onmsg = DefaultOnMsg,
					msgs = {
						---@param s Obj
						["write-line"] = function(_, s, _)
							if s.ctx["-type"] == "string" then print(s.ctx["-value"])
							elseif s.ctx["-type"] == "number" then print(s.ctx["-value"])
							elseif s.ctx["-type"] == "boolean" then print(s.ctx["-value"])
							elseif s.ctx["-type"] == "block" then
								print("<block:" .. formatAst(s.ctx["-value"]) .. ">")
							else print("<object:"..tostring(s)..">") end
							return makeNilObj()
						end,
						["read-line"] = function(_, _, _)
							local out = io.read()
							return makeStringObj(out)
						end
					}
				}
			end
		}
	}
}

--\[ Eval ]------------------------------------------------------------------------------------------
--/[ Output ]----------------------------------------------------------------------------------------

local function dumpAst(o, i)
	i = i or 0
	if type(o) == 'table' then
		local s = ''
		if o.t then s = o.t .. ' ' end
		s = s .. '{ '

		local keycount = 0
		for k, _ in pairs(o) do
			if k ~= "t" then keycount = keycount + 1 end
		end

		if keycount > 1 then s = s .. '\n' end

		for k, v in pairs(o) do
			if k ~= "t" then
				if keycount > 1 then s = s .. ("  "):rep(i + 1) end
				s = s .. k .. ' = ' .. dumpAst(v, i + 1) .. ','
				if keycount > 1 then s = s .. '\n' end
			end
		end
		if keycount > 1 then return s .. ("  "):rep(i) .. '}'
		else return s .. ' }' end
	elseif type(o) == "string" then
		return '"' .. o .. '"'
	else
		return tostring(o)
	end
end

SECTION_SUFFIX_LENGTH = 95

--- print section separator. returns `not closed`.
---@param name string name of the section
---@param closed boolean? whether the section is closed
---@return boolean
local function printSection(name, closed)
	print("\x1b[1;35m"
		.. ((closed and ".") or "-")
		.. "-[ " .. name
		.. ((closed and "\x1b[0;30m hidden\x1b[1;35m") or "")
		.. " ]-"
		.. ("-"):rep(SECTION_SUFFIX_LENGTH - #name - ((closed and #" hidden") or 0))
		.. ((closed and "") or "\n") .. "\x1b[0m")
	return not closed
end

-----------------------------------------------------------------------------------------------------
--/[ Example ]---------------------------------------------------------------------------------------

---@param input string
---@param sections boolean|{input: boolean, tokens: boolean, ast: boolean, rec: boolean, eval: boolean}
local function program(input, sections)
	local tokens = lex(input)
	if sections == true or sections ~= false then print() end

	if sections ~= false and printSection("Input", sections == true or sections.input) then
		print(input)
	end

	if sections ~= false and printSection("Tokens", sections == true or sections.tokens) then
		for _, t in ipairs(tokens) do
			print(t.t, t.v)
		end
	end

	local ast = parse(tokens)

	if sections ~= false and printSection("AST", sections == true or sections.ast) then
		print(dumpAst(ast))
		print()
	end

	if sections ~= false and printSection("Reconstructed Input", sections == true or sections.rec) then
		print(formatAst(ast, true))
		print()
	end

	if sections == false or printSection("Eval", sections == true or sections.eval) then
		eval(ast, Builtins)
	end
end

if #arg == 0 then
	print("usage: lua self.lua <filename>")
else
	local f, err = io.open(arg[1], "r")
	if not f then
		print("error reading file: " .. err)
	else
		program(f:read("a"), false)
		f:close()
	end
end

-- >w< --
