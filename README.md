# tinyself

> tiny message-based programming language made in lua.

![image](/image.png)

## example

```clojure
var console = (system get-console -)
console write-line "Hello, World!"
var inp = (console read-line -)

if true (inp equals "r/pl is cool") then [
  console write-line "based"
] else-if (inp equals "") then [
  console write-line "enter the secret sentence :)"
] else [
  console write-line "no secret :("
]
```

## syntax

The syntax is rather simple. Informal grammar:

```
newline = '\n'                          - newline

identifier = /[a-zA-Z][a-zA-Z-]*/       - identifier
           | '-'                        - nil literal

string-literal = /"([^"]|(\\[nte'"]))"/ - string literal
number-literal = /[0-9]+/               - number literal

atom = identifier                       - identifier
     | string-literal                   - string
     | number-literal                   - number
     | '(' newline* expression ')'      - parenthesised expression
     | '[' newline* expression ']'      - block

expression = newline* atom
           | expression newline* identifier newline* atom
```

> **Note** that all of the characters not recognised by the lexer are ignored!
> That means you can write `var x = y` and it will mean `var x y` :)

## explanation + examples

### objects

Objects are the basic unit in the language. An object contains some message handlers and a context. A message handler is a chunk of code (tinyself code or lua code) that is invoked when we send a message to the object. The object's context is it's internal state (table of objects accessed by some identifier).

Messages are identified by identifiers! If a message exists in an object, that means that that object has a message handler for that message.

### primitives

There are 3 basic primitives in the language:

- numbers: Positive integers (TODO: floats)
- strings: Strings.
- blocks: A block stores code. Has its own context when evaluated. Example: `[a b c]`

### message passing

```
system get-console -
system exit (1 plus 2)
```

In the first line, we pass a message called `get-console` to the `system` object with a `nil` argument. That returns the `console` object that can be used for IO.

In the second line, we first pass a message called `plus` to `1` with the argument `2`. That returns a `3`. After that, we pass a message called `exit` to `system` with an argument that was calculated earlier (`3`).

You can "recursively" send multiple messages like this for example:
```
system get-console - write-line "hello!"
```

### variables

Variables don't exist in the core language! They are implemented through the `var` object. It is a special object that captures any message and creates a variable with the name of that message and the value of the passed argument. For example:

```js
var x 3
```

This, as with any other object, passed a message called `x` to the `var` object with the argument `3`. *But*, due to how `var` works, it creates a variable called `x` with the value `3`!

As said earlier, because the lexer ignores unknown characters, we can write this:

```js
var x = 3
```

This feature was discovered by accident by the way B)

### cloning

To create new objects, we clone others. This is achieved by sending a `-` message to any object (note: this is special, you can even clone `var`) with any argument (TODO!)

The most basic cloning operation you can do is cloning a nil object: `- - -`.

> Note: because the lexer doesn't allow `-` at the beginning of identifiers, we can write this without spaces: `---`

### setting a message handler

> **NB:** This can and probably will change in the future.

To make our newly cloned object do anything, we need to add message handlers to it. That is done by sending a non-existent message to an object with a block argument. Example:

```js
var my-object = (---)
my-object my-message [console write-line "hello"]
my-object my-message -
```

First we create a variable `my-object` and set it to a clone of `nil`. Next we send a message called `my-message` to our clone. Because a message handler doesn't exist for that message yet, it is created for us. When we send `my-message` to our object in the next line, our block is invoked with the argument (`nil` in this case) being stored in a variable called `@` (**TODO:** you cannot currently access that variable... yeah... might choose a different name). A variable called `self` also becomes available inside of the block. That variable is a context object with the context from the object we sent the message to.

### context objects

> **NB:** this will be changed

Context objects are objects that allow you to access variables from a context (for example an object context). They behave similar to `var` in the sense that they don't have any specific message handlers, they use a message's name as a name of a variable.

- **Getting:** `ctx name -` this will return an object from the context \[object\] `ctx` with the name `name`.
- **Setting:** `(ctx --) name value` this will set an object from the context \[object\] `ctx` with the name `name` to `value`.

## stdlib

The standard library doesn't include a lot of things currently.

> note: this syntax and these types currently only exist in this document.

> ### syntax:
> ```
> tname ::= /[A-Z][a-z\-]*/ | '-'
> gtname ::= tname ('[' gtname (',' gtname)* ']')?
> type ::= tname
>        | type '[' type (',' type)* ']'
>        | type '|' type
>        | '(' type ')'
> name ::= /[a-z][a-z\-]*/ 
> gname ::= name ('[' gtname (',' gtname)* ']')?
> msg ::= gname type ':' type
> ```

### global objects

> - `gname (':' type)?` - description

- `system : System` - stuff
- `var` - variable setting
- `true : Boolean` - boolean literal
- `false : Boolean` - boolean literal
- `if : If_` - allows for basic control flow

### types

> - `gtname` - description
>   - `msg` - description
>   - `msg` - description
>   - ...

- `System` - stuff
  - `get-console - : Console` - returns a console object
  - `exit Number : -` - exits the process with an error code
- `Console` - standard IO
  - `write-line[T] T : -` - writes a line to the stdout
  - `read-line - : String` - read a line from the stdin
- `Number` - number type
  - `equals[T] T : Boolean` - compare self to another object
  - `plus Number : Number` - add self to another number
  - `minus Number : Number` - substract another number from self
  - `times Number : Number` - multiply self and another number
  - `divided-by Number : Number` - divide self by another number
- `String` - string type
  - `equals[T] T : Boolean` - compare self to another object
- `Boolean` - boolean type
  - `equals[T] T : Boolean` - compare self to another object
  - `and Boolean : Boolean` - logical and between two booleans
  - `or Boolean : Boolean` - logical or between two booleans
- `Block[I, O]` - block type
  - `equals[T] T : Boolean` - always returns false
  - `invoke I : O` - invoke the block in context
  - `in Context : Block[I, O]` - create new block with two merged contexts
- `Context` - context type
- `If_` - type of the `if` object
  - `true[T] T : If[T, -]` - sets the first condition
- `If[T, R]` - control flow logic
  - `then[S] Block[T, S] : If[T, R|S]` - sets the branch if the condition is truthy
  - `else-if[U] U : If[T|U, R]` - sets the condition
  - `else[S] Block[-, S] : R|S` - invokes the the correct branch

## semantics

> This section isn't done yet.

The semantics are also not very complicated. Let's start by introducing some things:

- **Context.** table from strings to objects. Can have a *parent context*.
- **Object.** runtime collection of message handlers and a context.

### contexts

> Internally are just tables. See `Ctx` type for reference.

When accessing or setting a value inside of a context, it is recursively searched for, with the parent context being searched last.

### objects

> See `Obj` type for reference.

Table of message handlers and a context.
